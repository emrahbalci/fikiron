{
    let $ = global.$ || {};
    let jQuery = global.jQuery || {};

    $ = jQuery = require('jquery');

    global.$ = $;
    global.jQuery = jQuery;

	require('flexslider');
	require('jquery-validation');


    var length = $('.mainSlider .slides li').length,
        classNames = '';
    for (var i = 0; i < length+1; i++) {
        classNames += ' slide-' + i
    }
	$('.mainSlider').flexslider({
        useCSS: false,
        animation: "fade",
        slideshow: true,
        animationLoop: true,
        slideshowSpeed: 4000,
        animationSpeed: 700,
        pauseOnAction: false,
        pauseOnHover: true,
        directionNav: false,
        controlNav: false,
        after: function () {
            var number = Number($('.mainSlider li.flex-active-slide').index());
            $('.slideTexts').removeClass(classNames).addClass('slide-' + (++number));
        }
    });

	$('.sectionSlider').flexslider({
        useCSS: false,
        animation: "fade",
        slideshow: true,
        animationLoop: true,
        slideshowSpeed: 7000,
        animationSpeed: 1,
        pauseOnAction: false,
        pauseOnHover: true,
        directionNav: true,
        controlNav: true
    });

	//if dom load
	$(window).on('load', function () {
		$('body').addClass('loaded');
    });

	$(window).on('load scroll', function () {
        var offsetTop = $('.sliderOverlay').innerHeight();
        var scrollTop = $(window).scrollTop();

        if (scrollTop < offsetTop) {
            $('.sliderOverlay').css('opacity', scrollTop / offsetTop);
        }

        $('section.fullscreen').each(function () {
            var $this = $(this);
            if (scrollTop > $this.offset().top-100) {
                $this.addClass('animate');
            } else if ( scrollTop == 0) {
                // $this.removeClass('animate');
            }
        });
    });

    //Takım detay slider
    $('.teamDetailSlider').flexslider({
        useCSS: false,
        animation: "slide",
        touch: false,
        smoothHeight: true,
        slideshow: false,
        animationLoop: true,
        slideshowSpeed: 7000,
        animationSpeed: 600,
        pauseOnAction: false,
        pauseOnHover: true,
        directionNav: false,
        controlNav: false,
        startAt: window.location.hash.substr(1),
        customDirectionNav: $('.navigation a')
    });

    $(document).on('click', '.navigation .flex-next', function(e) {
        e.preventDefault();
        $('.teamDetailSlider:eq(0)').flexslider('next');
        $('.teamDetailSlider:eq(1)').flexslider('next')
    });

    $(document).on('click', '.navigation .flex-prev', function(e) {
        e.preventDefault();
        $('.teamDetailSlider:eq(0)').flexslider('prev');
        $('.teamDetailSlider:eq(1)').flexslider('prev')
    });

    $('.megaMenuButton').on('click', function () {
        $('body').toggleClass('menuActive');
    });

    $('.portfolioItem:not(.noHover)').on('click touchstart', function () {
        $('.portfolioItem').removeClass('is-flipped');
        $(this).toggleClass('is-flipped')
    });

    $('.form input, .form textarea').on('change', function () {
        $(this).val().length > 0 ? $(this).parent().addClass('success') : $(this).parent().removeClass('success')
    });

    $('.faqList dt').on('click', function () {
        $(this).siblings('dd').slideUp().siblings('dt').removeClass('active');
        if (!$(this).hasClass('active')) {
            $(this).toggleClass('active');
            $(this).next('dd').slideToggle();
        }
    });
};